export const environment = {
  production: false,
  base_url: 'https://d3v-server-cf-air-api.ikoob.co.kr/api/v1',
  socket_url: 'https://d3v-server-cf-air-socket-api.ikoob.co.kr',
  eventId: '60f8ce727d8eac00126eff17' // SCRMI2021
};
