export const environment = {
  production: true,
  base_url: 'https://cfair-api.cf-air.com/api/v1', // 운영서버
  socket_url: 'https://cfair-socket-ssl.cf-air.com',
  eventId: '60f8ce727d8eac00126eff17' // SCRMI2021
};
