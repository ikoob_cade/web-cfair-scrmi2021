import * as _ from 'lodash';
import { _ParseAST } from '@angular/compiler';
import { Component, OnInit, AfterViewInit, HostListener, ViewChild, ElementRef } from '@angular/core';
import { SpeakerService } from '../../services/api/speaker.service';
import { BoothService } from '../../services/api/booth.service';
import { CookieService } from 'ngx-cookie-service';
import { CategoryService } from '../../services/api/category.service';
import { EventService } from '../../services/api/event.service';

declare var $: any;

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit, AfterViewInit {
  @ViewChild('entryAlertBtn') entryAlert: ElementRef;
  public speakers: Array<any> = [];
  public sponsors: Array<any> = []; // 스폰서 목록
  public selectedBooth: any;
  public mobile: boolean;

  public programBookUrl = '';
  public excerptUrl = '';

  public user;
  public event;
  public attachments = []; // 부스 첨부파일
  public booths: Array<any> = []; // 카테고리[부스] 목록

  constructor(
    private speakerService: SpeakerService,
    private boothService: BoothService,
    private cookieService: CookieService,
    private categoryService: CategoryService,
    private eventService: EventService
  ) { }


  @HostListener('window:resize', ['$event'])
  onResize(event): void {
    // if (window.innerWidth < 768 !== this.mobile) {
    //   this.loadSpeakers();
    // }
  }

  /**
   * 배열 나누기
   * @param array 배열
   * @param n n개씩 자르기
   */
  division = (array, n) => {
    return [].concat.apply([],
      array.map((item, i) => {
        return i % n ? [] : [array.slice(i, i + n)]
      })
    );
  }

  ngOnInit(): void {
    this.user = sessionStorage.getItem('cfair');
    this.checkEventVersion();
    // this.loadSpeakers();
    this.loadCategories();
    // this.loadBooths();
  }

  // 버전확인
  checkEventVersion(): void {
    this.eventService.findOne().subscribe(res => {
      this.event = res;
      if (this.event.clientVersion) {
        const clientVersion = localStorage.getItem(this.event.eventCode + 'ver');
        if (!clientVersion) {
          localStorage.setItem(this.event.eventCode + 'ver', this.event.clientVersion);
          location.reload();
        } else if (clientVersion) {
          if (clientVersion !== this.event.clientVersion) {
            localStorage.setItem(this.event.eventCode + 'ver', this.event.clientVersion);
            location.reload();
          }
        }
      }
    });
  }


  ngAfterViewInit(): void {
    // if (this.cookieService.get(`${environment.eventId}_main_popup`)) {
    //   let cookieTime = this.cookieService.get(`${environment.eventId}_main_popup`);
    //   const now = new Date().getTime().toString();
    //   if (cookieTime < now) {
    //     this.cookieService.delete(`${environment.eventId}_main_popup`);
    //     this.policyAlertBtn.nativeElement.click();
    //   }
    // } else {

    // ! 메인 팝업
    // setTimeout(() => {
    // this.entryAlert.nativeElement.click();
    // }, 0);
  }

  loadBooths = () => {
    this.boothService.find().subscribe(res => {
      this.booths = res.slice(0, 14);

      // const categories =
      //   _.chain(res)
      //     .groupBy(booth => {
      //       return booth.category ? JSON.stringify(booth.category) : '{}';
      //     })
      //     .map((booth, category) => {
      //       category = JSON.parse(category);
      //       category.booths = booth;

      //       // console.log('category= ', category)
      //       return category;
      //     }).sortBy(category => {
      //       return category.seq;
      //     })
      //     .value();

      // this.booths = categories;
    });
  }

  // 발표자 리스트를 조회한다.
  loadSpeakers(): void {
    const limit = 6;
    this.speakerService.find(false, true).subscribe(res => {
      this.speakers = this.division(res, limit);
    });
  }

  loadCategories = () => {
    this.categoryService.find('speaker').subscribe(res => {
      const categories = res;

      const keynoteSpeakers = [
        categories[0].speakers[1], categories[0].speakers[0],
        categories[1].speakers[0], categories[1].speakers[2],
        categories[1].speakers[1], categories[0].speakers[2]];
      const speakers = keynoteSpeakers;

      _.forEach(categories, category => {
        if (category.id !== '60fe5b53fa289d0013f61e7a' && category.id !== '60fe5b76fa289d0013f61e7d') {
          category.speakers = _.sortBy(category.speakers, 'seq');
          _.forEach(category.speakers, speaker => {
            speakers.push(speaker);
          });
        }
      });

      this.speakers = this.division(speakers, 6);
    });
  }

}
