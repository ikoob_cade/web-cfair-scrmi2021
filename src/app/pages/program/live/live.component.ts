import * as _ from 'lodash';
import { Component, OnInit, ViewChild, ElementRef, OnDestroy, HostListener, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { forkJoin as observableForkJoin, Observable } from 'rxjs';
import { AgendaService } from '../../../services/api/agenda.service';
import { DateService } from '../../../services/api/date.service';
import { RoomService } from '../../../services/api/room.service';
import { map } from 'rxjs/operators';
import { MemberService } from '../../../services/api/member.service';
import { HistoryService } from '../../../services/api/history.service';
import { SocketService } from '../../../services/socket/socket.service';
import { BannerService } from '../../../services/api/banner.service';
import { NgImageSliderComponent } from 'ng-image-slider';
import { DomSanitizer } from '@angular/platform-browser';
declare var $: any;

@Component({
  selector: 'app-live',
  templateUrl: './live.component.html',
  styleUrls: ['./live.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LiveComponent implements OnInit, OnDestroy {
  @ViewChild('entryAlertBtn') entryAlertBtn: ElementRef;
  @ViewChild('bannerSlider') bannerSlider: NgImageSliderComponent;

  public user: any; // 사용자 정보
  public live: any; // 라이브 방송
  public isLiveAgenda: any; // 현재 진행중인 아젠다

  public attendance: boolean; // 출석상태

  public dates: any[] = []; // 날짜 목록
  public rooms: any[] = []; // 룸 목록

  public agendas: any[] = []; // 날짜/룸으로 필터링 된 강의.

  public liveChatUrl;

  public selected: any = {
    date: null,
    room: null
  };

  public showPlayer = false;

  public banners = []; // 광고배너 리스트
  public bannersLiveChat = []; // 플레이어에 넘겨줄 채팅창 위 롤링배너 데이타
  public classColMd = false;

  public next: any;

  constructor(
    public router: Router,
    public route: ActivatedRoute,
    private dateService: DateService,
    private roomService: RoomService,
    private agendaService: AgendaService,
    private memberService: MemberService,
    private historyService: HistoryService,
    private socketService: SocketService,
    private bannerService: BannerService,
    private sanitizer: DomSanitizer,
  ) {
    this.doInit();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event): void {
    if (window.innerWidth < 1070) {
      this.classColMd = true;
    } else {
      this.classColMd = false;
    }
  }

  ngOnInit(): void {
    this.user = JSON.parse(sessionStorage.getItem('cfair'));
    this.getBanners();

    if (window.innerWidth < 1070) {
      this.classColMd = true;
    } else {
      this.classColMd = false;
    }
  }

  doInit(): void {
    this.historyService.setAttendance('in');
    const observables = [this.getDates(), this.getRooms()];
    observableForkJoin(observables)
      .pipe(
        map(resp => {
          this.dates = resp[0];
          this.rooms = resp[1];
          return resp;
        })
      )
      .subscribe(res => {
        let date = this.dates[0];
        let room = this.rooms[0];
        this.route.queryParams.subscribe(param => {
          // 선택된 날짜/룸이 있다면 해당 날짜와 룸이 선택될 수 있도록 한다.
          if (param.dateId && param.roomId) {
            date = this.dates.find(date => {
              return date.id === param.dateId;
            });
            room = this.rooms.find(room => {
              return room.id === param.roomId;
            });
          }
          this.router.navigate([], { queryParams: { dateId: null, roomId: null }, queryParamsHandling: 'merge' });
        });
        this.selected = { date, room };
        this.setAgendaList(date, room);
      });
  }

  // Date/Room 선택 시 Agenda Filtering
  setAgendaList(date: any, room: any, next?: boolean): any {
    this.historyService.setRoom(room);

    /*
    * 날짜의 옵션값 확인해서 player컴포넌트 출력을 제어한다.
    * 선택한 날짜의 ON/OFF 상태를 확인해서 플레이어, 채팅 이용 불가하도록 하기 위함
    */
    if (!date.showPlayer) {
      this.showPlayer = false;
    } else {
      this.showPlayer = true;
    }

    setTimeout(() => {
      this.socketService.leave(this.selected.room.id);
      this.socketService.join(room.id);
    }, this.user ? 0 : 300);

    this.user.dateId = date.id;
    this.user.roomId = room.id;

    if (this.live && !this.historyService.isAttended()) {
      this.openEntryAlert();
      if (!this.showPlayer) {
        this.showPlayer = !this.showPlayer;
      }

      this.next = () => {
        this.setAgendaList(date, room, true);
      };
    } else {
      if (next) {
        this.next = null;
      }

      setTimeout(() => {
        this.selected = {
          date,
          room
        };
        this.getAgendasV2(date.id, room.id)
          .subscribe(agendas => {
            this.agendas = agendas;
            setTimeout(() => {
              this.live = this.selected.room.contents;

              // 별도의 채팅창 포함 여부 확인
              if (this.live && this.live.chatIncluded && this.live.chatUrl) {
                this.liveChatUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.live.chatUrl);
              }

              // Live가 있으면 IN / OUT
              if (this.live) {
                if (this.showPlayer) {
                  this.openEntryAlert();
                }
                this.checkLiveAgenda();
              }
            }, 100);
          });
        this.live = null; // new app-player
      }, 500);
    }
  }

  /**
   * 입장/퇴장 모달을 출력한다.
   * user의 isLog데이터 구분.
   * 해외연자는 연수평점의 의무가 없어서 입장/퇴장이 불필요하다.
   */
  openEntryAlert(): void {
    // 로그 저장하지 않는 경우
    if (!this.user.isLog) {
      return;
    } else {
      // 퇴장 팝업은 생략하고 서버로 전송한다.
      if (this.getAttendance() === 'out') {
        this.atndnCheck();
      } else {
        this.entryAlertBtn.nativeElement.click();
      }
    }
  }

  // 현재 진행중인 아젠다 확인
  checkLiveAgenda(): void {
    this.isLiveAgenda = this.agendas[0];
    this.agendas.forEach((agenda: any) => {
      const agendaDate = agenda.date.date.replace(/-/gi, '/');
      const agendaStartTime = new Date(`${agendaDate}/${agenda.startTime}`);
      const agendaEndTime = new Date(`${agendaDate}/${agenda.endTime}`);
      const now: Date = new Date();
      if (now > agendaStartTime && now < agendaEndTime) {
        this.isLiveAgenda = agenda;
      }
    });
  }

  /* 아젠다 목록 조회 V2
   * date, room 값을 파라미터에 추가했음.
   * 위 조건에 해당하는 아젠다만 받아온다 => 서버 부하 감소
   */
  getAgendasV2(dateId, roomId): Observable<any> {
    return this.agendaService.findV2(dateId, roomId);
  }

  /** 날짜 목록 조회 */
  getDates(): Observable<any> {
    return this.dateService.find();
  }

  /** 룸 목록 조회 */
  getRooms(): Observable<any> {
    return this.roomService.find();
  }

  /**
   * 강의 상세보기
   * @param agenda 강의
   */
  goDetail(agenda: any): void {
    this.router.navigate([`/live/${this.selected.date.id}/${this.selected.room.id}/${agenda.id}`]);
  }

  // 출석체크
  atndnCheck(isMove?): void {
    if (!this.user.isLog) {
      // 로그 저장 안함
      return;
    }

    const options: { relationId: string, relationType: string, logType: string } = {
      relationId: this.selected.room.id,
      relationType: 'room',
      logType: this.historyService.getAttendance()
    };

    sessionStorage.setItem('currentRoom', this.selected.room.id);
    this.memberService.history(this.user.id, options)
      .subscribe(() => {
        if (this.historyService.isAttended()) {
          this.historyService.setAttendance('out');
        } else {
          this.historyService.setAttendance('in');
        }

        if (this.next) {
          this.next();
        }

        if (isMove) {
          this.openEntryAlert();
        }
      },
        (error) => {
          console.error(error);
        });
  }

  // 참여 상태 출력
  getAttendance(): string {
    return this.historyService.isAttended() ? 'in' : 'out';
  }


  // ! -------------------  Banner Slider  -------------------
  // 슬라이드 배너 신경외과용이니 나중에 복사한다.
  /** 배너목록을 조회한다. */
  getBanners(): void {
    this.bannerService.find().subscribe(res => {
      if (res.live) {
        res.live.forEach(item => {
          const data = {
            link: item.link,
            thumbImage: item.photoUrl,
            alt: item.title,
          };
          this.banners.push(data);
        });
      }
      // const bannersLiveChat = [];
      // res.liveChat.forEach(item => {
      //   const data = {
      //     link: item.link,
      //     thumbImage: item.photoUrl,
      //     alt: item.title,
      //   };
      //   bannersLiveChat.push(data);
      // });

      // if (bannersLiveChat.length > 0) {
      //   this.bannersLiveChat = bannersLiveChat;
      // }
    });
  }

  // 광고 구좌 왼쪽 버튼 클릭
  slidePrev(target): void {
    this[target].prev();
  }
  // 광고 구좌 오른쪽 버튼 클릭
  slideNext(target): void {
    this[target].next();
  }
  // 광고 배너 클릭
  imageClick(index): void {
    if (this.banners[index] && this.banners[index].link) {
      window.open(this.banners[index].link);
    }
  }

  /**
   * 페이지 나갈 때 처리
   */
  ngOnDestroy(): void {
    this.socketService.leave(this.selected.room.id);
    this.historyService.setRoom(null);
  }

  /**
   * 이 페이지 벗어나기 전에(window:beforeunload) history('out')을 위한 이벤트 리스너
   * @param $event 이벤트
   */
  @HostListener('window:beforeunload', ['$event'])
  public beforeunloadHandler($event): any {
    // 로그 저장하는 경우
    if (!this.historyService.isAttended() && this.user.isLog) {
      this.atndnCheck(true);
    }

    $event.preventDefault();
    return false;
  }
}
