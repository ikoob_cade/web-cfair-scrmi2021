import * as _ from 'lodash';
import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { VodService } from '../../../services/api/vod.service';
import { SpeakerService } from '../../../services/api/speaker.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-vod',
  templateUrl: './vod.component.html',
  styleUrls: ['./vod.component.scss']
})
export class VodComponent implements OnInit, AfterViewInit {

  public categories: Array<any>;
  public selectSpeaker: any;
  @ViewChild('quizWarningBtn') quizWarningAlert: ElementRef;

  constructor(
    private vodService: VodService,
    private speakerService: SpeakerService,
    public router: Router
  ) { }

  ngOnInit(): void {
    this.loadVods();
  }

  ngAfterViewInit(): void {
    // this.quizWarningAlert.nativeElement.click();
  }

  /**
   * VOD 목록 조회
   */
  loadVods = () => {
    this.vodService.find().subscribe(res => {
      const categories = this.sortByCategory(res);
      categories.forEach((category) => {
        const groups = {};
        category.vods.forEach((vod) => {
          if (vod.group) {
            if (!groups[vod.group]) {
              groups[vod.group] = [vod];
            } else {
              groups[vod.group].push(vod);
            }
          }
        });
        const groupList = [];

        // tslint:disable-next-line: forin
        for (const i in groups) {
          groupList.push(groups[i]);
        }

        if (groupList.length > 0) {
          category.groups = groupList;
        }
      });
      this.categories = categories;
    });
  }

  /**
   * 각 VOD에 카테고리가 있을 경우 카테고리별로 정렬해서 return
   */
  sortByCategory = (vods) => {
    return _.chain(vods)
      .groupBy(vod => {
        return vod.category ? JSON.stringify(vod.category) : '{}';
      })
      .map((vod, category) => {
        category = JSON.parse(category);
        category.vods = vod;
        return category;
      }).sortBy(category => {
        return category.seq;
      })
      .value();
  }


  goDetail(vod): void {
    this.router.navigate([`/vod/${vod.id}`]);
  }

  /**
   * 발표자 LIVE / VOD 리스트 조회
   */
  getDetail = (selectSpeaker) => {
    this.speakerService.findOne(selectSpeaker.id)
      .subscribe(res => {
        this.selectSpeaker = res;
      });
  }
}
