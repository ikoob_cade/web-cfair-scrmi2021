import * as _ from 'lodash';
import { UAParser } from 'ua-parser-js';
import * as moment from 'moment-timezone';
import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { VodService } from '../../../services/api/vod.service';
import { NgImageSliderComponent } from 'ng-image-slider';
import { BannerService } from '../../../services/api/banner.service';
import { MemberService } from '../../../services/api/member.service';
import { HistoryService } from '../../../services/api/history.service';
import { QuizService } from '../../../services/api/quiz.service';

declare let $: any;
@Component({
  selector: 'app-vod-detail',
  templateUrl: './vod-detail.component.html',
  styleUrls: ['./vod-detail.component.scss']
})
export class VodDetailComponent implements OnInit, AfterViewInit {
  @ViewChild('bannerSlider') bannerSlider: NgImageSliderComponent;
  @ViewChild('quizNoticeBtn') quizNoticeAlert: ElementRef;
  @ViewChild('quizWarningBtn') quizWarningAlert: ElementRef;

  public banners = [];

  /** 정답 확인 후 여부 */
  public beforeSubmit = true;

  /** 현재 퀴즈 순번: 0~ */
  public currentQuizTab = 0; //

  /** 선택한 보기배열 (다중선택) */
  public selectedAnswers;

  /** 선택한 보기 (단일선택) */
  public selectedSingleAnswer = null;

  /** 정답 여부 */
  public isCorrect = null;

  public next: any;
  public quizzes;
  private user = JSON.parse(sessionStorage.getItem('cfair'));

  public isSingleCorrect = true;
  public originVod;

  public vod: any;
  public vodId: string;


  selectedVod: any = [];
  public newInput;

  private myAnswers = [];

  constructor(
    private location: Location,
    private activatedRoute: ActivatedRoute,
    public vodService: VodService,
    private bannerService: BannerService,
    private memberService: MemberService,
    private quizService: QuizService,
    private historyService: HistoryService,
  ) { }

  ngOnInit(): void {
    this.historyService.setAttendance('in');

    this.activatedRoute.params.subscribe(params => {
      if (params.vodId) {
        this.vodService.findOne(params.vodId).subscribe(vod => {
          this.originVod = vod;
          this.selectedVod = this.originVod;

          this.vod = vod.contents;
          this.vodId = vod.id;
          this.quizzes = vod.quizzes;

        });
        // this.getBanners();
      }
    });
  }

  ngAfterViewInit(): void {
    // this.quizWarningAlert.nativeElement.click();
  }

  // ! Banner Slider
  // slidePrev(target): void {
  //   this[target].prev();
  // }

  // slideNext(target): void {
  //   this[target].next();
  // }

  // imageClick(index): void {
  //   if (this.banners[index] && this.banners[index].link) {
  //     window.open(this.banners[index].link);
  //   }
  // }

  // 리스트로 돌아가기
  goBack(): void {
    this.location.back();
  }

  // ! QUIZ
  public quizNoticeModal(): void {
    if (this.originVod.isCompleted !== 1) {
      this.quizNoticeAlert.nativeElement.click();
    }
  }

  closeQuiz(): void {
    if (this.originVod.isCompleted === 2 || this.originVod.isFinished) {
      return $('#quizModal').modal('hide');
    }

    if (window.confirm('창을 닫은 후, 다시 문제 풀이를 하려면\n처음부터 강의를 들으셔야 합니다.​\n정말 닫으시겠습니까?')) {
      $('#quizModal').modal('hide');
    }
  }

  /**
   * 퀴즈모달 팝업 출력
   */
  openQuizModal(isOpen?): void {
    this.quizService.checkQuizTime().subscribe(res => {
      if (!res) {
        const quizStartDate = moment('2021-07-25 09:00:00').tz('Asia/Seoul');
        const quizEndDate = moment('2021-07-26 00:00:00').tz('Asia/Seoul');

        return alert('사용가능 시간:\n' + quizStartDate.format('YYYY-MM-DD HH:mm A') + ' ~ ' + quizEndDate.format('YYYY-MM-DD HH:mm A'));
      } else {
        // 이미 모두맞춘 정답제출 데이터 존재
        if (this.originVod.isCompleted === 1) {
          return alert('이미 완료한 퀴즈입니다.');
        }

        // 제출한 이력이 없는 경우
        if (this.originVod.isCompleted === 0 && !isOpen && !this.originVod.isFinished) {
          return alert('영상이 끝난 후 자동팝업됩니다.');
        }

        this.currentQuizTab = 0;

        this.initQuiz();

        $('#quizModal').modal('show');
      }
    });
  }

  /**
   * 정답 확인
   */
  submitQuiz(): void {
    if (!this.isSingleCorrect) {
      if (_.filter(this.selectedAnswers, exam => exam).length < 1) {
        return alert('보기를 선택해 주세요.');
      }

      // this.quizSubmit(this.quizzes[this.currentQuizTab].id, this.selectedAnswers);

    } else {
      if (this.selectedSingleAnswer === null) {
        return alert('보기를 선택해 주세요.');
      }

      let quizId = this.quizzes[this.currentQuizTab].id;
      this.quizSubmit(quizId, this.selectedSingleAnswer, null);
    }
  }

  /**
   * 제출 초기화
   */
  resetQuiz(): void {
    this.beforeSubmit = true;
  }

  // 다음문제 or 제출
  nextQuiz(): void {
    if (!this.quizzes[this.currentQuizTab + 1]) {
      this.quizComplete();
    } else {
      this.currentQuizTab++;

      this.initQuiz(true);
    }
  }

  previousQuiz(): void {
    this.currentQuizTab--;
    this.initQuiz();
  }

  initQuiz(next?): void {
    if (!next) {
      this.myAnswers = [];
    }

    this.beforeSubmit = true;
    this.selectedSingleAnswer = null;
    this.isSingleCorrect = this.quizzes[this.currentQuizTab].isSingleCorrect;

    // 보기 선택배열 초기화
    this.selectedAnswers = [];

    for (let i = 0; i < this.quizzes[this.currentQuizTab].quizContent.length; i++) {
      this.selectedAnswers.push(false);
    }

    if (!this.selectedAnswers.length) {
      this.selectedAnswers = null;
    }
  }

  // ! HTTP
  /** 배너목록을 조회한다. */
  getBanners(): void {
    this.bannerService.find().subscribe(res => {
      if (_.keys(res).length > 0) {
        res.vod.forEach(item => {
          const data = {
            link: item.link,
            thumbImage: item.photoUrl,
            alt: item.title,
          };
          this.banners.push(data);
        });
      }
    });
  }

  // 퀴즈 정답 확인
  quizSubmit(quizId, selectedSingleAnswer?, selectedAnswers?): void {
    const options: any = {};

    if (selectedSingleAnswer !== null) {
      options.selectedSingleAnswer = selectedSingleAnswer;
    }

    this.quizService.quizSubmit(quizId, options).subscribe(res => {
      if (!this.myAnswers[this.currentQuizTab]) {
        this.myAnswers.push(this.selectedSingleAnswer);
      }

      this.isCorrect = res.result;
      this.beforeSubmit = false;
    });
  }

  // 퀴즈 완료
  quizComplete(): void {
    const body = {
      relationId: this.originVod.id,
      type: 'vod',
      quizIds: _.map(this.quizzes, 'id'),
      myAnswers: this.myAnswers
    };

    this.memberService.quizComplete(this.user.id, body).subscribe(res => {
      alert('제출이 완료되었습니다.​\n문제풀이 여부는 마이페이지에서 확인이 가능합니다.​');
      $('#quizModal').modal('hide');
    });
  }

  // ! 댓글
  getVodComments(): void {
    this.selectedVod.comments = { commentList: [], count: 0 };

    this.vodService.findComments(this.vodId).subscribe(res => {
      this.selectedVod.comments = res;
    });
  }

  /** 댓글 생성 */
  createComment(parentId?: string): void {
    let parser = new UAParser();
    let fullUserAgent = parser.getResult();

    if (parentId) {
      if (!$(`#cInput_${parentId}`)[0].value) {
        return alert('질문을 입력하세요.');
      }
    } else if (!this.newInput) {
      return alert('질문을 입력하세요.');
    }

    const body: any = {
      description: parentId ? $(`#cInput_${parentId}`)[0].value : this.newInput,
      memberId: this.user.id,
      parentId: parentId ? parentId : '',
      level: parentId ? 1 : 0,
      userAgent: JSON.stringify(fullUserAgent),
      browser: JSON.stringify(fullUserAgent.browser),
      device: JSON.stringify(fullUserAgent.device),
      engine: JSON.stringify(fullUserAgent.engine),
      os: JSON.stringify(fullUserAgent.os),
      ua: JSON.stringify(fullUserAgent.ua),
    };

    this.vodService.createComment(this.vodId, body).subscribe(res => {
      this.newInput = '';
      this.getVodComments();
    });
  }

  /** 답글 Input 활성화 */
  openReply(selectedCommentId): void {
    const cInputWrapper = $(`#cInput_wrapper_${selectedCommentId}`)[0];
    if (cInputWrapper.style.display === 'none' || !cInputWrapper.style.display) {
      cInputWrapper.style.display = 'block';
      $(`#cInput_${selectedCommentId}`)[0].focus();
    } else {
      cInputWrapper.style.display = 'none';
    }
  }

  /** 댓글 수정 활성화 */
  modify(inputId): void {
    const input = $(`#${inputId}`)[0];
    input.disabled = false;
    input.focus();
  }

  update(commentId, type): void {
    let input;
    if (type === 'child') {
      input = $(`#cInput_${commentId}`)[0];
    } else {
      input = $(`#pInput_${commentId}`)[0];
    }

    if (!input.value) {
      return alert('댓글을 입력하세요.');
    }

    const body = {
      description: input.value,
    };

    this.vodService.updateComment(this.vodId, commentId, body).subscribe(res => {
      this.getVodComments();
    });
  }

  isModify(inputId): boolean {
    if ($(`#${inputId}`) && $(`#${inputId}`)[0]) {
      return !$(`#${inputId}`)[0].disabled;
    }
    return false;
  }

  cancel(): void {
    this.getVodComments();
  }

  /** 댓글을 삭제한다 */
  remove(commentId): void {
    if (confirm('댓글을 삭제하시겠습니까?')) {
      this.vodService.deleteComment(this.vodId, commentId).subscribe(res => {
        this.getVodComments();
      });
    }
  }

}
