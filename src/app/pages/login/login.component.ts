import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AuthService } from '../../services/auth/auth.service';
import { Router } from '@angular/router';
import { UAParser } from 'ua-parser-js';
declare var $: any;

enum AUTH_ERROR_MESSAGE {
  UNAUTHORIZED = 'Email과 Password가 일치하지 않습니다.',
  NOT_FOUND = 'Email과 Password가 일치하지 않습니다.',
  ENTER_ALL = 'Please complete all fields',
}
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {
  @ViewChild('policyAlertBtn') policyAlertBtn: ElementRef;
  private user: any;
  public passwordError = ''; // 로그인 에러메세지
  public member = {
    email: '',
    password: '',
  };

  public pwdEmail = ''; // 패스워드 찾기의 이메일 input

  constructor(
    public router: Router,
    public auth: AuthService) { }

  ngOnInit(): void {
  }

  login(): void {
    if (this.loginValidator()) {
      const parser = new UAParser();
      const fullUserAgent = parser.getResult();

      const body: any = {
        email: this.member.email,
        password: this.member.password,
        userAgent: JSON.stringify(fullUserAgent),
        browser: JSON.stringify(fullUserAgent.browser),
        device: JSON.stringify(fullUserAgent.device),
        engine: JSON.stringify(fullUserAgent.engine),
        os: JSON.stringify(fullUserAgent.os),
        ua: JSON.stringify(fullUserAgent.ua),
      };

      this.auth.login(body).subscribe(async res => {
        if (res.token) {
          this.user = res;
          this.passwordError = '';
          this.policyAlertBtn.nativeElement.click();

          // this.goToMain();
        }
      }, error => {
        if (error.status === 401) {
          this.passwordError = AUTH_ERROR_MESSAGE.UNAUTHORIZED;
        }
        if (error.status === 404) {
          this.passwordError = AUTH_ERROR_MESSAGE.NOT_FOUND;
        }
      });
    } else {
      alert(AUTH_ERROR_MESSAGE.ENTER_ALL);
    }
  }

  loginValidator(): boolean {
    this.member.email = this.member.email.trim();
    if (!this.member.email || !this.member.password) {
      return false;
    }
    return true;
  }

  /**
   * 로그인 정보를 스토리지 저장 및 메인으로 이동.
   */
  goToMain(): void {
    sessionStorage.setItem('cfair', JSON.stringify(this.user));
    this.router.navigate(['/main']);
  }


  /**
   * 초기화 이메일을 전송한다.
   */
  // sendEmail(): void {
  //   if (this.pwdEmail) {
  //     this.auth.sendResetEmail(this.pwdEmail).subscribe(res => {
  //       if (res) {
  //         alert('해당 이메일로 임시 비밀번호가 전송되었습니다.');
  //         this.cancelFindPwd();
  //       }
  //     }, error => {
  //       if (error.status === 404) {
  //         alert('해당 계정을 찾을 수 없습니다.');
  //       }
  //     });
  //   } else {
  //     alert('이메일을 입력해 주세요.');
  //   }
  // }

  // /**
  //  * 비밀번호 찾기 닫기
  //  */
  // cancelFindPwd = () => {
  //   $('#findPassword').modal('hide');
  //   this.pwdEmail = '';
  // }
}
