import { Component, OnInit } from '@angular/core';
import { BoardService } from '../..//services/api/board.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss']
})
export class BoardComponent implements OnInit {

  public search: FormGroup;
  public curPage: number = 1;
  public boards: any[] = [];

  public pages: any[] = [];
  public totalCount: number = 0;
  public totalPagesCount: number = 0;

  constructor(
    private fb: FormBuilder,
    private boardService: BoardService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.search = this.fb.group({
      type: [''],
      keyword: ['']
    })

    this.OnInit();
  }

  OnInit() {
    this.getBoards();
  }

  // 모든 게시글 불러오기
  getBoards() {

    let params = this.search.value;
    params.page = this.curPage;
    return this.boardService.find(params).subscribe((resp: any) => {
      this.boards = resp.boards;
      this.totalCount = resp.totalCount;
      this.totalPagesCount = resp.totalPagesCount;
      this.pages = Array(resp.totalPagesCount).fill(resp.totalPagesCount).map((x, i) => i + 1);
    });
  }

  submit() {
    this.curPage = 1;
    this.getBoards();
  }

  setPage(page) {
    if (page < this.curPage) {
      if (this.curPage === 1) return;
      this.curPage--;
      this.getBoards();
    } else {
      if (this.totalPagesCount === this.curPage) return;
      this.curPage++;
      this.getBoards();
    }
  }

  goDetail(board) {
    // if (board.isFixed && board.type !== 'notice' && !sessionStorage.getItem('cfair')) {
    //   alert('Login is required.');
    //   return this.router.navigate(['/login']);
    // }

    this.router.navigate(['/board/' + board.id]);
  }

}
