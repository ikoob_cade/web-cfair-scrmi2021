import * as _ from 'lodash';
import { Component, OnInit, HostListener } from '@angular/core';
import { BoothService } from 'src/app/services/api/booth.service';
import { _ParseAST } from '@angular/compiler';
import { SponsorService } from 'src/app/services/api/sponsor.service';

@Component({
  selector: 'app-sponsors',
  templateUrl: './sponsors.component.html',
  styleUrls: ['./sponsors.component.scss']
})
export class SponsorsComponent implements OnInit {

  public sponsors: Array<any> = []; // 스폰서 목록
  public mobile: boolean = false;

  constructor(
    private boothService: BoothService,
  ) { }

  ngOnInit(): void {
    this.loadSponsors();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.mobile = window.innerWidth < 768;
  }

  /**
   * 스폰서 목록 조회
   * 스탬프 투어를 위해 Booth 데이터 사용한다. 2020.10.19
   */
  loadSponsors = () => {
    this.boothService.find().subscribe(res => {
      this.sponsors = res;
      // console.log('GET Sponsors', this.sponsors);
    });
  }
}
