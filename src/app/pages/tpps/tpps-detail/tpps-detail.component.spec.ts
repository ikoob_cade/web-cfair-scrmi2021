import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TppsDetailComponent } from './tpps-detail.component';

describe('PostersDetailComponent', () => {
  let component: TppsDetailComponent;
  let fixture: ComponentFixture<TppsDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TppsDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TppsDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
