import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class SponsorService {
  private serverUrl = '/events/:eventId/sponsors';

  constructor(private http: HttpClient) { }

  find(): Observable<any> {
    return this.http.get(this.serverUrl)
      .pipe(catchError(this.handleError));
  }

  findOne(sponsorId: string, memberId?: string): Observable<any> {
    const params = {
      params: {
        memberId: ''
      }
    };
    if (memberId) {
      params.params.memberId = memberId;
    }

    return this.http.get(this.serverUrl + '/' + sponsorId, params)
      .pipe(catchError(this.handleError));
  }

  private handleError(error: HttpErrorResponse): Observable<never> {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }

    // Return an observable with a user-facing error message.
    return throwError('Something bad happened; please try again later.');
  }

}
