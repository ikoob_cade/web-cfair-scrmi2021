import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BoardService {
  private serverUrl = '/events/:eventId/board';

  constructor(
    private http: HttpClient
  ) { }

  find(search?: { type: string, keyword: string, page: number }): Observable<any> {
    let url: string = '';
    let keys = Object.keys(search);
    keys.forEach(key => {
      if (search[key]) {
        url += !url.length ? '?' : '&';
        url += `${key}=${search[key]}`;
      }
    })
    return this.http.get(this.serverUrl + url)
      .pipe(catchError(this.handleError));
  }

  findOne(endpoint: string): Observable<any> {
    return this.http.get(`${this.serverUrl}/${endpoint}`)
      .pipe(catchError(this.handleError));
  }

  create(body: any): Observable<any> {
    return this.http.post(this.serverUrl, body)
      .pipe(catchError(this.handleError));
  }

  update(endpoint: string, body: any): Observable<any> {
    return this.http.put(`${this.serverUrl}/${endpoint}`, body)
      .pipe(catchError(this.handleError));
  }

  delete(endpoint): Observable<any> {
    return this.http.delete(`${this.serverUrl}/${endpoint}`)
      .pipe(catchError(this.handleError));
  }

  private handleError(error: HttpErrorResponse): Observable<never> {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }

    // Return an observable with a user-facing error message.
    return throwError('Something bad happened; please try again later.');
  }

}
