import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { environment } from '../../../environments/environment';

import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs/internal/observable/throwError';

@Injectable({
  providedIn: 'root'
})
export class AuthInterceptorService implements HttpInterceptor {

  constructor() { }

  public intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const BASE_URL: string = environment.base_url;
    const EVENT_ID: string = environment.eventId;

    request = request.clone({ url: BASE_URL + request.url });
    if (request.url.match(/:eventId/g)) {
      request = request.clone({
        url: request.url.replace(/:eventId/g, EVENT_ID)
      });
    }

    if (sessionStorage.getItem('cfair')) {
      let auth = JSON.parse(sessionStorage.getItem('cfair'));
      if (auth && auth.token) {
        request = this.addToken(request, auth.token);
      }
    }

    return next.handle(request).pipe(
      catchError(error => {
        //   if (error instanceof HttpErrorResponse && error.status === 401) {
        //     return this.handle401Error(request, next);
        //   } else {
        return throwError(error);
        //   }
      })
    );
  }
  
  private addToken(request: HttpRequest<any>, token: string) {
    return request.clone({
      setHeaders: { 'Authorization': `Bearer ${token}` }
    });
  }

}
