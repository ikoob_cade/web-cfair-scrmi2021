import { Component, OnInit, Input, OnChanges, ViewChild, ElementRef, AfterViewInit, ChangeDetectorRef, OnDestroy, ViewEncapsulation, HostListener, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { _ParseAST } from '@angular/compiler';
// import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';
import { MemberService } from '../../services/api/member.service';
import videojs from 'video.js';
import * as _ from 'lodash';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { BannerService } from '../../services/api/banner.service';
import { DataService } from '../../services/data.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { VideoJsOptions } from "../../common/videojs-options";
declare let $: any;
@Component({
  selector: 'app-player-vod',
  templateUrl: './player-vod.component.html',
  styleUrls: ['./player-vod.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class PlayerVodComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('videoPlayer') videoPlayer: ElementRef;
  @Input('content') content: any; // 전달받은 콘텐츠 정보
  @Input('vodId') vodId: any; // 전달받은 vodId
  @Input('selected') selected: any; // 전달받은 날짜/룸(채널) 정보
  @ViewChild('commentList') commentList: any; // 댓글 목록

  @Output('openQuiz') openQuiz = new EventEmitter(); // 퀴즈팝업

  public isLive = false;
  public banner;

  public user: any;
  public reg = /vimeo.com/;

  public player: any;

  public replyForm: FormGroup;
  public replys: any = [];
  private relationId: string;

  private timerID;
  private vodWatch;
  public watchTime = 0;

  private isFirstPlay = true;

  private forceLogoutSubscription: Subscription;

  constructor(
    public fb: FormBuilder,
    private cdr: ChangeDetectorRef,
    private memberService: MemberService,
    public sanitizer: DomSanitizer,
    private bannerService: BannerService,
    private dataService: DataService,
    private router: Router
  ) {
    this.replyForm = fb.group({
      content: ['', Validators.compose([Validators.required, Validators.minLength(10)])]
    });
  }

  ngOnDestroy(): void {
    clearInterval(this.timerID);
    clearInterval(this.vodWatch);
    if (this.watchTime > 0) {
      this.insertHistory();
    }

    this.forceLogoutSubscription.unsubscribe();
  }

  ngOnInit(): void {
    this.relationId = this.content.id;

    this.user = JSON.parse(sessionStorage.getItem('cfair'));
    // this.getComment();
    // this.timerID = setInterval(() => {
    //   this.getComment();
    // }, 10 * 1000);
    this.forceLogoutSubscription = this.dataService.forceLogout.subscribe(data => {
      if (this.watchTime) {
        this.insertHistory(data);
      }
    });
  }

  ngAfterViewInit(): void {
    this.cdr.detectChanges();
    this.setVideoPlayer();
  }

  /** 비디오 플레이어 셋팅 */
  setVideoPlayer(): void {
    const opt: any = {
      controls: false,
      fluid: true,
      preload: 'auto',
      sources: [{
        src: this.content.contentUrl,
        type: 'video/vimeo',
      }],
    };

    this.player = new videojs(this.videoPlayer.nativeElement, opt, () => {
      const that = this;
      this.player.on('error', (error) => {
        this.offVideoEvent();
      });

      // 동영상 종료
      this.player.on('ended', () => {
        // this.insertHistory(null, true);
        // this.openQuiz.emit(null);
        that.player.initChildren();
      });

      // 동영상 재생
      this.player.on('play', () => {
        if (this.isFirstPlay) {
          this.insertHistory();
          this.isFirstPlay = false;
        }

        this.vodWatch = setInterval(() => {
          this.watchTime++;
        }, 1000);
      });

      // 동영상 일시정지
      this.player.on('pause', () => {
        clearInterval(this.vodWatch);
      });
    });
  }

  // 비디오 이벤트 제거
  offVideoEvent = () => {
    if (this.player) {
      this.player.off('ended');
      this.player.off('play');
      this.player.off('pause');
      this.player.off('error');
    }
  }

  /** 댓글 불러오기 */
  getComment(): void {
    this.memberService.findComment(this.user.id, this.relationId).subscribe(res => {
      this.replys = res;
    });
  }

  /** 댓글달기 */
  comment(): void {
    this.memberService.createComment({
      memberId: this.user.id,
      title: '',
      content: this.replyForm.value.content,
      relationId: this.relationId
    }).subscribe(res => {
      this.getComment();
      // this.replyForm.value.content = '';
      this.replyForm.patchValue({
        content: ''
      });

      // this.downScroll();
    });
  }

  /** 채팅창을 최하단으로 스크롤한다. */
  downScroll(): void {
    this.commentList.nativeElement.scrollTop = this.commentList.nativeElement.scrollHeight;
  }

  /** 본인이 입력한 채팅 여부 (삭제 출력) */
  check(comment): boolean {
    if (comment && comment.memberId === this.user.id) {
      return true;
    }
    return false;
  }

  /** 채팅을 삭제한다. */
  delete(comment): void {
    if (confirm('Are you sure you want to delete?')) {
      this.memberService.deleteComment(this.user.id, comment.id)
        .subscribe(res => {
          this.getComment();
        });
    }
  }

  // 본 시간(초) 만큼 이력 전송.
  insertHistory(event?, isEnd?): void {
    const options: { relationId: string, watchTime: number } = {
      relationId: this.vodId,
      watchTime: this.watchTime,
    };
    // 서버에 저장
    this.memberService.historyOfVod(
      this.user.id,
      options).subscribe(res => {
        this.watchTime = 0;

        if (isEnd) {
          clearInterval(this.vodWatch);
        }
        /**
         * 기존에는 컴포넌트 destory시에만 시간을 적재하였으나,
         * 브라우저가 최소화 되있을경우 라우터 동작 시 멈춰버리는 현상 발견.
         * 시청이력 오류가 없도록 최소한 로그제출 한뒤에 라우트한다.
         */
        if (event) {
          if (event === 1) { // ! 중복로그인
            // 전송 후에 토큰 remove
            sessionStorage.removeItem('cfair');
            alert('Sign in from another device.');
          }

          if (event === 2) { // !행사종료
            alert('금일 행사가 종료되었습니다.');
          }
          this.router.navigate(['/']);
        }
      });
  }

  /**
   * 이 페이지 벗어나기 전에(window:beforeunload) 시청시간 전송을 위한 이벤트 리스너
   * @param $event 이벤트
   */
  @HostListener('window:beforeunload', ['$event'])
  public beforeunloadHandler($event): any {

    // 로그 저장하는 경우
    if (this.watchTime > 0) {
      this.insertHistory();
    }
    this.watchTime = 0;

    $event.preventDefault();
    // tslint:disable-next-line: deprecation
    ($event || window.event).returnValue = '로그아웃 하시겠습니까?';
    return false;
  }
}
