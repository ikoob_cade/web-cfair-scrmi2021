import { Component, OnInit, Input, HostListener, ViewChild, ElementRef, ChangeDetectorRef, AfterContentChecked, OnDestroy } from '@angular/core';
import { forkJoin as observableForkJoin, Observable } from 'rxjs';
import { DateService } from '../../services/api/date.service';
import { RoomService } from '../../services/api/room.service';
import { Router } from '@angular/router';
import * as moment from 'moment';

@Component({
  selector: 'app-time-table',
  templateUrl: './time-table.component.html',
  styleUrls: ['./time-table.component.scss']
})
export class TimeTableComponent implements OnInit, AfterContentChecked, OnDestroy {

  @ViewChild('table1', { read: ElementRef }) table1: ElementRef;
  @ViewChild('th1', { read: ElementRef }) th1: ElementRef;
  @ViewChild('th2', { read: ElementRef }) th2: ElementRef;
  @ViewChild('th3', { read: ElementRef }) th3: ElementRef;
  // tslint:disable-next-line: no-input-rename
  @Input('agendas') agendas: any;

  public date = 1;

  private koreaTimer;
  public koreaTime = moment().utc().utcOffset('+0900'); // 한국 시간 : UTC +0900;


  public channelBtnStyle1: any;
  public channelBtnStyle2: any;
  public channelBtnStyle3: any;
  constructor(
    private router: Router,
    private cdr: ChangeDetectorRef,
    private dateService: DateService,
    private roomService: RoomService,
  ) { }

  ngOnInit(): void {
    this.doInit();
    this.getTime();
  }

  ngAfterContentChecked(): void {
    this.cdr.detectChanges();
    this.setChannelBtnStyle();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event): void {
    this.setChannelBtnStyle();
  }

  setDate = (index: number) => {
    this.date = index;
  }

  setChannelBtnStyle(): void {
    switch (this.date) {
      case 1:
        this.channelBtnStyle1 = {
          height: this.table1.nativeElement.clientHeight - this.th1.nativeElement.clientHeight + 'px',
          top: this.th1.nativeElement.clientHeight + 'px'
        };
        break;
      default:
        break;
    }
  }

  private dates: any;
  private rooms: any;

  doInit(): void {
    const observables = [this.getDates(), this.getRooms()];
    observableForkJoin(observables)
      .subscribe(resp => {
        this.dates = resp[0];
        this.rooms = resp[1];
      });
  }

  /** 날짜 목록 조회 */
  getDates(): Observable<any> {
    return this.dateService.find();
  }

  /** 룸 목록 조회 */
  getRooms(): Observable<any> {
    return this.roomService.find();
  }

  /**
   * Live 상세보기
   * @param date 순서
   * @param room 순서
   */
  goLive(date: number, room: number): void {
    this.router.navigate(['/live'], { queryParams: { dateId: this.dates[date].id, roomId: this.rooms[room].id } });
  }

  goPoster(): void {
    this.router.navigate(['/posters']);
  }

  goVod(): void {
    this.router.navigate(['/vod']);
  }

  public scrollToTable(): void {
  }

  getTime(): void {
    this.koreaTimer = setInterval(() => {
      this.koreaTime = moment().utc().utcOffset('+0900'); // 한국 시간 : UTC +0900
    }, 1000);
  }


  ngOnDestroy(): void {
    clearInterval(this.koreaTimer);
  }


}
