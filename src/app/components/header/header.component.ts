import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { MemberService } from '../../services/api/member.service';
import { AuthService } from '../../services/auth/auth.service';
import { EventService } from '../../services/api/event.service';
import { SocketService } from '../../services/socket/socket.service';

declare var $: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {
  public user: any;
  public menus: any = [
    { title: 'WELCOME', router: '/about' },
    { title: 'PROGRAM', router: '/program' },
    { title: 'LIVE', router: '/live' },
    { title: 'SPEAKERS', router: '/speakers' },
    // { title: 'POSTERS', router: '/posters' },
    { title: 'SPONSORS', router: '/e-booth' },
    { title: 'NOTICE', router: '/board' },
  ];

  collapse = true;

  event;

  constructor(
    private router: Router,
    private eventService: EventService,
    private authService: AuthService,
    private memberService: MemberService,
    private socketService: SocketService,
  ) {
    router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe((val) => {
        const auth = JSON.parse(sessionStorage.getItem('cfair'));
        this.user = (auth && auth.token) ? auth : undefined;
        // console.log(this.user)

        if (this.user) {
          this.loginSocket();
        }

        $('.navbar-collapse').collapse('hide');
        this.collapse = true;
      });
  }

  ngOnInit(): void {
    this.getEvent();
  }

  getEvent() {
    this.eventService.findOne().subscribe(res => {
      this.event = res;
    })
  }

  ngOnDestroy(): void {
  }

  postAttendance = (): void => {
    this.memberService.attendance(this.user.id).subscribe(res => { });
  }

  /** 로그아웃 */
  logout(): void {
    this.authService.logout().subscribe(res => {
      this.logoutSocket();

      sessionStorage.removeItem('cfair');
      this.router.navigate(['/']);
    }, error => {
      sessionStorage.removeItem('cfair');
      this.router.navigate(['/']);
    });
  }

  loginSocket() {
    this.socketService.login({
      memberId: this.user.id,
      token: this.user.token
    });
  }

  logoutSocket() {
    this.socketService.logout({
      memberId: this.user.id
    });
  }

  openKakao(): void {
    window.open('http://pf.kakao.com/_rxnSfs/chat', '_blank');
  }

}
