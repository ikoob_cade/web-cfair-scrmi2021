import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-program-live',
  templateUrl: './program-live.component.html',
  styleUrls: ['./program-live.component.scss']
})

export class ProgramLiveComponent implements OnInit {
  // tslint:disable-next-line:no-input-rename
  @Input('program-live') programLive;
  // tslint:disable-next-line:no-input-rename
  @Input('speakerId') speakerId;

  constructor(
    private router: Router,
  ) { }

  ngOnInit(): void {
  }

  /**
   * 라이브로 이동한다.
   * @param agenda 아젠다
   */
  goLive(agenda): void {
    this.router.navigate(['/live'], { queryParams: { dateId: agenda.dateId, roomId: agenda.roomId } });
  }
}
