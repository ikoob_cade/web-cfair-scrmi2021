import { Component, OnInit, Input, ViewChild, ElementRef, AfterViewInit, ChangeDetectorRef, OnDestroy, ViewEncapsulation, } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { _ParseAST } from '@angular/compiler';
// import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';
import '@devmobiliza/videojs-vimeo/dist/videojs-vimeo.esm';
import { MemberService } from '../../services/api/member.service';
import videojs from 'video.js';
import * as _ from 'lodash';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { NgImageSliderComponent } from 'ng-image-slider';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class PlayerComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('playerContainer') playerContainer: ElementRef;
  @ViewChild('videoPlayer') videoPlayer: ElementRef;
  @Input('content') content: any; // 전달받은 콘텐츠 정보
  @Input('isLive') isLive = false; // 댓글 유무 확인 (상세에서는 채팅이 없기때문에 false로 받는다.);
  @Input('isVod') isVod = false;
  @Input('selected') selected: any; // 전달받은 날짜/룸(채널) 정보
  @ViewChild('commentList') commentList: any; // 댓글 목록

  @Input('isAgendaDetail') isAgendaDetail = false;

  @ViewChild('bannerSlider') bannerSlider: NgImageSliderComponent;

  @Input('bannersLiveChat') bannersLiveChat;

  // @Input('isVod') isVod = false;
  public banner;

  public user: any;
  public videoPlayerObject;
  public reg = /vimeo.com/;
  public player: any;

  public liveUrl;

  public excerptUrl = '';

  public replyForm: FormGroup;
  public replys: any = [];
  private relationId: string;

  private timerID;

  constructor(
    public fb: FormBuilder,
    private cdr: ChangeDetectorRef,
    private memberService: MemberService,
    private sanitizer: DomSanitizer,
  ) {
    this.replyForm = fb.group({
      content: ['', Validators.compose([Validators.required, Validators.minLength(10)])]
    });
  }

  ngOnDestroy(): void {
    clearInterval(this.timerID);
  }

  ngOnInit(): void {
    this.liveUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.content.contentUrl);
    this.relationId = this.isLive ? this.selected.room.id : this.content.id;

    this.user = JSON.parse(sessionStorage.getItem('cfair'));

    this.getComment();
    this.timerID = setInterval(() => {
      this.getComment();
    }, 10 * 1000);
  }

  ngAfterViewInit(): void {
    this.cdr.detectChanges();
    // this.setVideoPlayer();
  }

  /** 댓글 불러오기 */
  getComment(): void {
    this.memberService.findComment(this.user.id, this.relationId).subscribe(res => {
      this.replys = res;
    });
  }

  /** 댓글달기 */
  comment(): void {

    this.memberService.createComment({
      memberId: this.user.id,
      title: '',
      content: this.replyForm.value.content,
      relationId: this.relationId
    }).subscribe(res => {
      this.getComment();
      // this.replyForm.value.content = '';
      this.replyForm.patchValue({
        content: ''
      });

      // this.downScroll();
    });
  }

  /** 채팅창을 최하단으로 스크롤한다. */
  downScroll(): void {
    this.commentList.nativeElement.scrollTop = this.commentList.nativeElement.scrollHeight;
  }

  /** 본인이 입력한 채팅 여부 (삭제 출력) */
  check(comment): boolean {
    if (comment && comment.memberId === this.user.id) {
      return true;
    }
    return false;
  }

  /** 채팅을 삭제한다. */
  delete(comment): void {
    if (confirm('Are you sure you want to delete?')) {
      this.memberService.deleteComment(this.user.id, comment.id)
        .subscribe(res => {
          this.getComment();
        });
    }
  }

  /** 비디오 플레이어 셋팅 */
  setVideoPlayer(): void {
    if (this.reg.test(this.liveUrl)) {  // Vimeo
      this.useVimeo();
    } else {
      this.useMpeg();
    }
  }

  /**
   * 비메오 live 사용
   */
  useVimeo(): void {
    const opt: any = {
      controls: false,
      fluid: true,
      html5: { nativeTextTracks: false },
      preload: 'auto',
      sources: {
        src: this.liveUrl,
        type: 'live/vimeo'
      },
    };
    this.player = new videojs(this.videoPlayer.nativeElement, opt, () => {
      const that = this;
      this.player.on('error', (error) => {
        console.log('error', error);
        this.offVideoEvent();
      });

      // 동영상 재생 위치 변경 감지
      this.player.on('loadeddata', () => {
        console.log('loadeddata');
        that.player.off('loadeddata');
      });

      // 플레이어 초기 지속 시간 발생 감지
      this.player.on('loadedmetadata', () => {
        console.log('loadedmetadata');
        that.player.off('loadedmetadata');
      });

      // 동영상 종료
      this.player.on('ended', () => {
        console.log('ended');
        that.player.off('ended');
        that.player.initChildren();
      });

      // 동영상 재생
      this.player.on('play', () => {
        console.log('play');
      });

      // 동영상 일시정지
      // 기존에 일시정지하면 바로 서버에 로그를 기록했는데, 클라이언트에서 10초 간격으로 계산하여 서버에 요청하는 구조로 바뀜.
      // 이력은 그대로 냅둔다.
      this.player.on('pause', () => {
        console.log('pause');
      });
    });
  }

  /**
  * m3u8 전용
  */
  useMpeg(): void {
    const opt: any = {
      fluid: true,
      html5: { nativeTextTracks: true },
      preload: 'auto',
      sources: {
        src: this.liveUrl,
        type: 'application/x-mpegURL'
      },
      controls: true,
    };

    this.player = new videojs(this.videoPlayer.nativeElement, opt, () => {
      const that = this;
      this.player.on('error', (error) => {
        console.log('error');
        this.offVideoEvent();
      });

      // 동영상 재생 위치 변경 감지
      this.player.on('loadeddata', () => {
        console.log('loadeddata');
        that.player.off('loadeddata');
      });

      // 플레이어 초기 지속 시간 발생 감지
      this.player.on('loadedmetadata', () => {
        console.log('loadedmetadata');
        that.player.off('loadedmetadata');
      });

      // 동영상 종료
      this.player.on('ended', () => {
        console.log('ended');
        that.player.off('ended');
        that.player.initChildren();
      });

      // 동영상 재생
      this.player.on('play', () => {
        console.log('play');
      });

      // 동영상 일시정지
      this.player.on('pause', () => {
        console.log('pause');
      });
    });

    /**
     * m3u8
     * https://jlab-live.xst.kinxcdn.com/jlab_live/will01/playlist.m3u8
     * 구버전에는 정상작동 되나 현 프로젝트에서 controls속성이 html의 video태그 내에 선언이 되어있지만
     * 모듈이 해당 video 태그에 플레이어 생성 후 controls속성이 빠져있는것 확인.
     * 증상: 플레이어에 컨트롤바가 없어서 전체화면이나 재생/일시정지 등의 동작이 불가능
     *
     * 해결: 해당 Element를 화인해서 controls속성을 주입했음.
     * 2020. 10. 23(금)
     */
    if (!this.videoPlayer.nativeElement.hasAttribute('controls')) {
      this.videoPlayer.nativeElement.setAttribute('controls', 'controls');
    }
  }

  // 비디오 이벤트 제거
  offVideoEvent = () => {
    if (this.player) {
      this.player.off('loaded');
      this.player.off('ended');
      this.player.off('play');
      this.player.off('pause');
      this.player.off('seeked');
      this.player.off('seekable');
      // this.player.off('timeupdate');
    }
  }

  openBoard() {
    window.open('/board', '_blank');
  }
}
